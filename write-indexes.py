#!/usr/bin/env python3

import fdroid_metrics
import os
import sys


def main():
    outdir = 'public'
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    os.chdir(outdir)
    domains = sys.argv[1:]
    for domain in domains:
        git_repo = fdroid_metrics.get_metrics_data_repo(domain)
        fdroid_metrics.pull_commits(git_repo)
        fdroid_metrics.render_index_json(domain)
    fdroid_metrics.render_index_html(domains)


if __name__ == "__main__":
    main()
